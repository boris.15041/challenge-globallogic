FROM openjdk:11
COPY build/libs/challenge-0.0.1-SNAPSHOT.jar /app.jar
CMD ["java", "-jar", "-Dspring.profiles.active=test", "/app.jar"]