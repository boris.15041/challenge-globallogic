package com.globallogic.challenge.repository;

import com.globallogic.challenge.model.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static com.globallogic.challenge.data.UsersData.*;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserRepositoryTest {

    public final String EMAIL_USER = "example@gmail.com";

    public final String ID_USER_NEW = "ff8081817ade0f4b0o0s9o99dks990";
    public final String NAME_USER_NEW = "Agustin Emiliano";
    public final String LAST_NAME_USER_NEW = "Romero";
    public final String EMAIL_USER_NEW = "agustin@gmail.com";
    public final String PASSWORD_USER_NEW = "662asDD2";
    public final String TOKEN_USER_NEW = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaXNzIjoiZ2xvYmFsTG9naWMiLCJleHAiOjkwLCJpYXQiOjE2MjcyMTQ4ODUsImVtYWlsIjoicm91OXMzQGdtYWlsLmNvbSIsImp0aSI6IjFkZDM4NDM3LTJiNzEtNGQ4Yy04ZjhiLTNhZWUxM2QwOWIwOSJ9.Ym5KAV7W8qs4mUYuBK8dPmCGl_ikc4Rdn5zGrxWRf2k";
    public final LocalDateTime CREATED_USER_NEW = LocalDateTime.of(2021, 07, 23, 20, 15, 30);
    public final LocalDateTime MODIFIED_USER_NEW = LocalDateTime.of(2021, 07, 24, 22, 16, 31);
    public final LocalDateTime LAST_LOGIN_USER_NEW = LocalDateTime.of(2021, 07, 25, 23, 17,32);

    @Autowired
    private UserDAO userDAO;

    @Test
    @Order(1)
    void testFindById() {
        Optional<User> user = userDAO.findById(ID_USER_001);
        assertTrue(user.isPresent());
        assertEquals(NAME_USER_001, user.get().getName());
    }

    @Test
    @Order(2)
    void testFindByEmail() {
        Optional<User> user = userDAO.findByEmail(EMAIL_USER_002);
        assertTrue(user.isPresent());
        assertEquals(EMAIL_USER_002, user.get().getEmail());
    }

    @Test
    @Order(3)
    void testFindByEmailThrowException() {
        Optional<User> user = userDAO.findByEmail(EMAIL_USER);
        assertThrows(NoSuchElementException.class, () -> {
           user.get();
        });
        assertFalse(user.isPresent());
    }

    @Test
    @Order(4)
    void testFindAll() {
        List<User> users = userDAO.findAllByIsActiveIsTrue();
        assertFalse(users.isEmpty());
        assertEquals(2, users.size());
    }

    @Test
    @Order(5)
    void testSaveUser() {

        User user = new User();
        user.setName(NAME_USER_NEW);
        user.setLastName(LAST_NAME_USER_NEW);
        user.setEmail(EMAIL_USER_NEW);
        user.setPassword(PASSWORD_USER_NEW);
        user.setToken(TOKEN_USER_NEW);
        user.setCreated(CREATED_USER_NEW);
        user.setModified(MODIFIED_USER_NEW);
        user.setIsActive(true);

        user = userDAO.save(user);

        assertNotNull(user.getId());
        assertEquals(NAME_USER_NEW, user.getName());
        assertEquals(EMAIL_USER_NEW, user.getEmail());

    }

    @Test
    @Order(6)
    void testDeleteById() {
        User user = userDAO.findById(ID_USER_002).get();

        assertNotNull(user);
        assertEquals(NAME_USER_002, user.getName());
        assertEquals(LAST_NAME_USER_002, user.getLastName());

        userDAO.deleteById(user.getId());

        assertThrows(NoSuchElementException.class, () -> {
            userDAO.findById(ID_USER_002).get();
        });
        assertEquals(1, userDAO.findAll().size());
    }
}