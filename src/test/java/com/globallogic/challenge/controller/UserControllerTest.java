package com.globallogic.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globallogic.challenge.data.PhonesData;
import com.globallogic.challenge.dto.UserUnregisteredDTO;
import com.globallogic.challenge.exceptions.ConstantsException;
import com.globallogic.challenge.exceptions.EntityNotFoundException;
import com.globallogic.challenge.service.IUserService;
import com.globallogic.challenge.utils.ConstantsUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static com.globallogic.challenge.data.UsersData.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserService userService;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void testFindUserById() throws Exception {
        when(userService.findById(ID_USER_001)).thenReturn(getUserDTO001());

        Map<String, Object> response = new HashMap<>();
        response.put("name", NAME_USER_001 + " " + LAST_NAME_USER_001);
        response.put("email", EMAIL_USER_001);
        response.put("phones", PhonesData.getPhonesUser001());

        mockMvc.perform(get("/api/users/" + ID_USER_001).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(NAME_USER_001 + " " + LAST_NAME_USER_001))
                .andExpect(jsonPath("$.email").value(EMAIL_USER_001))
                .andExpect(content().json(objectMapper.writeValueAsString(response)));

        verify(userService).findById(ID_USER_001);
    }

    @Test
    void testFindAll() throws Exception {

        when(userService.findAll()).thenReturn(findAllUsers());

        mockMvc.perform(get("/api/users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value(NAME_USER_001 + " " + LAST_NAME_USER_001))
                .andExpect(jsonPath("$[0].email").value(EMAIL_USER_001))
                .andExpect(jsonPath("$[1].name").value(NAME_USER_002 + " " + LAST_NAME_USER_002))
                .andExpect(jsonPath("$[1].email").value(EMAIL_USER_002))
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(content().json(objectMapper.writeValueAsString(findAllUsers())));

        verify(userService).findAll();
    }

    @Test
    void testSaveUser() throws Exception {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantsUtils.FORMATTER_DATE);
        UserUnregisteredDTO userUnregisteredDTO = createUser001().get();

        when(userService.save(userUnregisteredDTO)).thenReturn(getUserSuccessDTO001());

        mockMvc.perform(post("/api/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(userUnregisteredDTO)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(ID_USER_001))
                .andExpect(jsonPath("$.created").value(CREATED_USER_001.format(formatter)))
                .andExpect(jsonPath("$.modified").value(MODIFIED_USER_001.format(formatter)))
                .andExpect(jsonPath("$.token").value(TOKEN_USER_001))
                .andExpect(jsonPath("$.last_login").value(LAST_LOGIN_USER_001.format(formatter)))
                .andExpect(jsonPath("$.isactive").value(true));

        verify(userService).save(createUser001().get());
    }

    @Test
    public void testDeleteUserById() throws Exception {

        doNothing().when(userService).deleteById(ID_USER_001);

        mockMvc.perform(delete("/api/users/" + ID_USER_001).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(userService).deleteById(ID_USER_001);

    }

}