package com.globallogic.challenge.service.impl;

import com.globallogic.challenge.data.PhonesData;
import com.globallogic.challenge.dto.UserDTO;
import com.globallogic.challenge.dto.UserSuccessDTO;
import com.globallogic.challenge.exceptions.EntityNotFoundException;
import com.globallogic.challenge.exceptions.UniqueEmailException;
import com.globallogic.challenge.model.User;
import com.globallogic.challenge.repository.UserDAO;
import com.globallogic.challenge.service.IUserService;
import com.globallogic.challenge.utils.ConstantsUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static com.globallogic.challenge.data.UsersData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class UserServiceImplTest {

    public final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantsUtils.FORMATTER_DATE);
    public final String ID_USER = "ff8081817ade0f4b017a2632665599266";

    @MockBean
    private UserDAO userDAO;

    @Autowired
    private IUserService userService;

    @Test
    void findAll() {
        when(userDAO.findAllByIsActiveIsTrue()).thenReturn(findAllUsersIsActive());

        List<UserDTO> usersDTO = userService.findAll();

        assertEquals(2, usersDTO.size());
        assertNotNull(usersDTO);
        assertNotNull(usersDTO.get(0).getPhones());
        assertNotNull(usersDTO.get(1).getPhones());
        assertEquals(NAME_USER_001 + " " + LAST_NAME_USER_001, usersDTO.get(0).getName());
        assertEquals(EMAIL_USER_001, usersDTO.get(0).getEmail());
        assertEquals(2, usersDTO.get(0).getPhones().size());
        assertEquals(NAME_USER_002 + " " + LAST_NAME_USER_002, usersDTO.get(1).getName());
        assertEquals(EMAIL_USER_002, usersDTO.get(1).getEmail());
        assertEquals(3, usersDTO.get(1).getPhones().size());

        verify(userDAO).findAllByIsActiveIsTrue();
    }

    @Test
    void findById() {

        UserDTO userFound;
        when(userDAO.findById(ID_USER)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
           userService.findById(ID_USER);
        });

        when(userDAO.findById(ID_USER_001)).thenReturn(getUser001());

        userFound = userService.findById(ID_USER_001).get();

        assertNotNull(userFound);
        assertEquals(NAME_USER_001 + " " + LAST_NAME_USER_001, userFound.getName());
        assertEquals(EMAIL_USER_001, userFound.getEmail());
        assertEquals(2, userFound.getPhones().size());
        verify(userDAO, times(2)).findById(anyString());

    }

    @Test
    void save() {

        UserSuccessDTO userSuccess;

        when(userDAO.findByEmail(EMAIL_USER_001)).thenReturn(getUser001());

        assertThrows(UniqueEmailException.class, () -> {
            userService.save(createUser001().get());
        });

        when(userDAO.findByEmail(EMAIL_USER_001)).thenReturn(Optional.empty());

        when(userDAO.save(any())).then(invocation -> {
           User user = invocation.getArgument(0, User.class);
           user.setId(ID_USER_001);
           user.setName(NAME_USER_001);
           user.setLastName(LAST_NAME_USER_001);
           user.setEmail(EMAIL_USER_001);
           user.setPassword(PASSWORD_USER_001);
           user.setToken(TOKEN_USER_001);
           user.setCreated(CREATED_USER_001);
           user.setModified(MODIFIED_USER_001);
           user.setIsActive(true);
           user.setPhones(PhonesData.getPhonesUser001());
           return user;
        });

        userSuccess = userService.save(createUser001().get()).get();

        assertNotNull(userSuccess);
        assertEquals(ID_USER_001, userSuccess.getId());
        assertEquals(CREATED_USER_001.format(formatter), userSuccess.getCreated().format(formatter));
        assertEquals(MODIFIED_USER_001.format(formatter), userSuccess.getModified().format(formatter));
        assertNotNull(userSuccess.getLastLogin());
        assertEquals(TOKEN_USER_001, userSuccess.getToken());
        assertTrue(userSuccess.getIsActive());

        verify(userDAO, times(2)).findByEmail(anyString());
        verify(userDAO).save(any());

    }

    @Test
    void update() {
    }

    @Test
    void deleteById() {

        when(userDAO.findById(ID_USER)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
            userService.deleteById(ID_USER);
        });

        when(userDAO.findById(ID_USER_001)).thenReturn(getUser001());
        when(userDAO.save(any())).then(invocation -> {
            User user = invocation.getArgument(0, User.class);
            user.setId(ID_USER_001);
            user.setName(NAME_USER_001);
            user.setLastName(LAST_NAME_USER_001);
            user.setEmail(EMAIL_USER_001);
            user.setPassword(PASSWORD_USER_001);
            user.setToken(TOKEN_USER_001);
            user.setCreated(CREATED_USER_001);
            user.setModified(MODIFIED_USER_001);
            user.setIsActive(false);
            user.setPhones(PhonesData.getPhonesUser001());
            return user;
        });

        userService.deleteById(ID_USER_001);

        verify(userDAO).findById(ID_USER);
        verify(userDAO).save(any());
    }
}