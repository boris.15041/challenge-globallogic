package com.globallogic.challenge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

@Configuration
public class SwaggerConfig {

    public static final Contact DEFAULT_CONTACT = new springfox.documentation.service.Contact(
            "Challenge Java - Global Logic", "https://www.linkedin.com/in/boris-cruz-15041/",
            "boris.15041@gmail.com");

    @SuppressWarnings("rawtypes")
    public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("CRUD Users",
            "Challenge Java - Global Logic", "1.0", "beta", DEFAULT_CONTACT,
            "Undefined", "Undefined", new ArrayList<>());

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO).select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

}
