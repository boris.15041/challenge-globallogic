package com.globallogic.challenge.controller;

import com.globallogic.challenge.dto.UserDTO;
import com.globallogic.challenge.dto.UserRegisteredDTO;
import com.globallogic.challenge.dto.UserSuccessDTO;
import com.globallogic.challenge.dto.UserUnregisteredDTO;
import com.globallogic.challenge.exceptions.EntityNotFoundException;
import com.globallogic.challenge.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping
    public ResponseEntity<List<UserDTO>> findAll() {
        log.info("Get all users");
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable String id) {
        log.info("Get user by id {}", id);
        return ResponseEntity.ok(userService.findById(id).get());
    }

    @PostMapping
    public ResponseEntity<UserSuccessDTO> saveUser(@RequestBody UserUnregisteredDTO userUnregistered) {
        log.info("Save user: {}", userUnregistered);
        return new ResponseEntity<>(userService.save(userUnregistered).get(), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<UserSuccessDTO> updateUser(@RequestBody UserRegisteredDTO userRegisteredDTO) {
        log.info("Update user: {}", userRegisteredDTO);
        return ResponseEntity.ok(userService.update(userRegisteredDTO).orElseThrow(() -> new EntityNotFoundException("User not found")));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUserById(@PathVariable String id) {
        log.info("Delete user with id: {}", id);
        userService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
