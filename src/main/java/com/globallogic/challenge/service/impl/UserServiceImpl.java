package com.globallogic.challenge.service.impl;

import com.globallogic.challenge.dto.UserDTO;
import com.globallogic.challenge.dto.UserRegisteredDTO;
import com.globallogic.challenge.dto.UserSuccessDTO;
import com.globallogic.challenge.dto.UserUnregisteredDTO;
import com.globallogic.challenge.exceptions.BadRequestException;
import com.globallogic.challenge.exceptions.ConstantsException;
import com.globallogic.challenge.exceptions.EntityNotFoundException;
import com.globallogic.challenge.exceptions.UniqueEmailException;
import com.globallogic.challenge.model.User;
import com.globallogic.challenge.repository.UserDAO;
import com.globallogic.challenge.service.IUserService;
import com.globallogic.challenge.utils.JwtToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@Transactional
public class UserServiceImpl implements IUserService {

    private final UserDAO userDAO;

    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public List<UserDTO> findAll() {
        return userDAO.findAllByIsActiveIsTrue().stream().collect(ArrayList::new, (list, element) -> {
            list.add(UserDTO.builder()
                    .withId(element.getId())
                    .withName(element.getName() + " " + element.getLastName())
                    .withEmail(element.getEmail())
                    .withPhones(element.getPhones())
                    .build());
        }, ArrayList::addAll);
    }

    @Override
    public Optional<UserDTO> findById(String id) {
        Optional<User> userFound = Optional.of(userDAO.findById(id).orElseThrow(() -> new EntityNotFoundException(User.class, id)));
        return userFound.map(user -> UserDTO.builder()
                .withName(user.getName() + " " + user.getLastName())
                .withEmail(user.getEmail())
                .withPhones(user.getPhones())
                .build()
        );
    }

    public Optional<User> save(User user) {
        return Optional.of(userDAO.save(user));
    }

    @Override
    public Optional<UserSuccessDTO> save(UserUnregisteredDTO userUnregisteredDTO) {
        userDAO.findByEmail(userUnregisteredDTO.getEmail())
                .ifPresent(user -> { throw new UniqueEmailException(user.getEmail());});
        User newUser = UserUnregisteredDTO.userUnregisteredDTO2User(userUnregisteredDTO);
        newUser.setToken(new JwtToken(newUser.getEmail()).toString());
        newUser = userDAO.save(newUser);
        return Optional.of(UserSuccessDTO.builder()
                .withId(newUser.getId())
                .withCreated(newUser.getCreated())
                .withModified(newUser.getModified())
                .withLastLogin(LocalDateTime.now())
                .withToken(newUser.getToken())
                .withIsActive(newUser.getIsActive())
                .build());
    }

    @Override
    public Optional<UserSuccessDTO> update(UserRegisteredDTO userRegisteredDTO) {
        if (userRegisteredDTO.getId() == null) {
            throw new BadRequestException("Id is required to update the User");
        }
        userDAO.findById(userRegisteredDTO.getId())
                .orElseThrow(() -> new EntityNotFoundException(ConstantsException.USER_NOT_FOUND_EXCEPTION + userRegisteredDTO.getId()));
        User updateUser = UserRegisteredDTO.userRegisteredDTO2User(userRegisteredDTO);
        updateUser.setToken(new JwtToken(updateUser.getEmail()).toString());
        return save(updateUser)
                .map(userUpdate -> UserSuccessDTO.builder()
                    .withId(userUpdate.getId())
                    .withCreated(userUpdate.getCreated())
                    .withModified(userUpdate.getModified())
                    .withLastLogin(LocalDateTime.now())
                    .withToken(userUpdate.getToken())
                    .withIsActive(userUpdate.getIsActive())
                    .build());
    }

    @Override
    public void deleteById(String id) {
        User userFound = userDAO.findById(id).orElseThrow(() -> new EntityNotFoundException(ConstantsException.USER_NOT_FOUND_EXCEPTION + id));
        userFound.setIsActive(false);
        userDAO.save(userFound);
    }
}
