package com.globallogic.challenge.service;

import com.globallogic.challenge.dto.UserDTO;
import com.globallogic.challenge.dto.UserRegisteredDTO;
import com.globallogic.challenge.dto.UserSuccessDTO;
import com.globallogic.challenge.dto.UserUnregisteredDTO;
import com.globallogic.challenge.exceptions.UniqueEmailException;
import com.globallogic.challenge.model.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    List<UserDTO> findAll();

    Optional<UserDTO> findById(String id);

    Optional<UserSuccessDTO> save(UserUnregisteredDTO userUnregisteredDTO);

    Optional<UserSuccessDTO> update(UserRegisteredDTO userRegisteredDTO);

    void deleteById(String id);

}
