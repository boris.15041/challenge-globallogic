package com.globallogic.challenge.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "USERS")
@Data
public class User implements Serializable {

    @Id
    @Column(length = 36)
    @GenericGenerator(name = "idGenerator", strategy = "uuid")
    @GeneratedValue(generator = "idGenerator")
    private String id;

    @Column(nullable = false, length = 100)
    @NotNull(message = "Name is a required")
    @NotEmpty(message = "Name is a required")
    @Size(max = 100, message = "Name cannot contain more than 100 characters")
    private String name;

    @Column(name = "LAST_NAME", nullable = false, length = 50)
    @NotNull(message = "Last name is a required")
    @NotEmpty(message = "Last name is a required")
    @Size(max = 50, message = "Last name cannot contain more than 50 characters")
    private String lastName;

    @Column(nullable = false, unique = true, length = 50)
    @NotNull(message = "Email is a required field")
    @NotEmpty(message = "Email is a required field")
    @Size(max = 25, message = "Email cannot contain more than 25 characters")
    @Pattern(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$", message="Email is invalid")
    private String email;

    @Column(nullable = false, length = 25)
    @NotNull(message = "Password is a required field")
    @NotEmpty(message = "Password is a required field")
    @Size(max = 25, message = "Password cannot contain more than 25 characters")
    @Pattern(regexp = "^(?=(?:.*\\d){2})(?=(?:.*[A-Z]))(?=(?:.*[a-z]))\\S{2,}$", message="Password requires at least one uppercase, one lowercase and two numbers")
    private String password;

    @Column(name = "IS_ACTIVE")
    @NotNull(message = "isActive is a required field")
    private Boolean isActive;

    @OneToMany(cascade=CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "USER_ID", foreignKey = @ForeignKey(name = "FK_NAME"))
    private List<Phone> phones;

    @Column(length = 356)
    private String token;

    @CreatedDate
    @Column(updatable = false)
    private LocalDateTime created;

    @LastModifiedDate
    private LocalDateTime modified;

    public User() {
        phones = new ArrayList<>();
    }
}
