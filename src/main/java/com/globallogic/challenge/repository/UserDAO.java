package com.globallogic.challenge.repository;

import com.globallogic.challenge.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDAO extends JpaRepository<User, String> {

    Optional<User> findByEmail(String email);

    List<User> findAllByIsActiveIsTrue();

}
