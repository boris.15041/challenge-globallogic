package com.globallogic.challenge.repository;

import com.globallogic.challenge.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneDAO extends JpaRepository<Phone, Long> {
}
