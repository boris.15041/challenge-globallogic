package com.globallogic.challenge.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import static com.globallogic.challenge.utils.ConstantsUtils.*;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder(setterPrefix = "with")
public class UserSuccessDTO {

    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMATTER_DATE)
    private LocalDateTime created;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMATTER_DATE)
    private LocalDateTime modified;

    @JsonProperty("last_login")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMATTER_DATE)
    private LocalDateTime lastLogin;

    private String token;

    @JsonProperty("isactive")
    private Boolean isActive;

}
