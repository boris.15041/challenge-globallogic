package com.globallogic.challenge.dto;

import com.globallogic.challenge.model.Phone;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder(setterPrefix = "with")
public class UserDTO {

    private String id;

    private String name;

    private String email;

    private List<Phone> phones;

}
