package com.globallogic.challenge.dto;

import com.globallogic.challenge.model.Phone;
import com.globallogic.challenge.model.User;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder(setterPrefix = "with")
public class UserUnregisteredDTO {

    private String name;

    private String lastName;

    private String email;

    private String password;

    private List<PhoneUnregisteredDTO> phones;

    public static User userUnregisteredDTO2User(UserUnregisteredDTO userUnregisteredDTO) {
        User newUser = new User();
        newUser.setName(userUnregisteredDTO.getName());
        newUser.setLastName(userUnregisteredDTO.lastName);
        newUser.setEmail(userUnregisteredDTO.getEmail());
        newUser.setPassword(userUnregisteredDTO.getPassword());
        newUser.setIsActive(true);
        newUser.setPhones(userUnregisteredDTO.getPhones().stream()
                .map(phoneDTO -> PhoneUnregisteredDTO.phoneUnregisteredDTO2Phone(phoneDTO))
                .collect(Collectors.toList())
        );
        return newUser;
    }

}
