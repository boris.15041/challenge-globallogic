package com.globallogic.challenge.dto;

import com.globallogic.challenge.model.User;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class UserRegisteredDTO {

    private String id;

    private String name;

    private String lastName;

    private String email;

    private String password;

    private List<PhoneUnregisteredDTO> phones;

    public static User userRegisteredDTO2User(UserRegisteredDTO userRegisteredDTO) {
        User user = new User();
        user.setId(userRegisteredDTO.getId());
        user.setName(userRegisteredDTO.getName());
        user.setIsActive(true);
        user.setLastName(userRegisteredDTO.lastName);
        user.setEmail(userRegisteredDTO.getEmail());
        user.setPassword(userRegisteredDTO.getPassword());
        user.setPhones(userRegisteredDTO.getPhones().stream()
                .map(phoneDTO -> PhoneUnregisteredDTO.phoneUnregisteredDTO2Phone(phoneDTO))
                .collect(Collectors.toList())
        );
        return user;
    }

}
