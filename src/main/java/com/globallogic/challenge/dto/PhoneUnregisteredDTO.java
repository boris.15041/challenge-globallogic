package com.globallogic.challenge.dto;

import com.globallogic.challenge.model.Phone;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(setterPrefix = "with")
public class PhoneUnregisteredDTO {

    private String number;

    private String cityCode;

    private String countryCode;

    public static Phone phoneUnregisteredDTO2Phone(PhoneUnregisteredDTO phoneUnregisteredDTO) {
        Phone phone = new Phone();
        phone.setNumber(phoneUnregisteredDTO.getNumber());
        phone.setCityCode(phoneUnregisteredDTO.getCityCode());
        phone.setCountryCode(phoneUnregisteredDTO.getCountryCode());
        return phone;
    }
}
