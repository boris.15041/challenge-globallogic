package com.globallogic.challenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class EntityAlreadyExistsException extends RuntimeException {

    public EntityAlreadyExistsException(Class<?> entity, Object value) {
        this("Entity [" + entity.getSimpleName() + "] already exist with id = " + value + "]");
    }

    public EntityAlreadyExistsException(String message) {
        super(message);
    }

}
