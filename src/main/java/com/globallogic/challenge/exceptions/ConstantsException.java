package com.globallogic.challenge.exceptions;

public class ConstantsException {
    public final static String USER_NOT_FOUND_EXCEPTION = "User not found with id: ";
}
