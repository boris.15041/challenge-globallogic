package com.globallogic.challenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class UniqueEmailException extends RuntimeException {

    public UniqueEmailException(String email) {
        super("The email [" + email + "] is already registered");
    }

}
