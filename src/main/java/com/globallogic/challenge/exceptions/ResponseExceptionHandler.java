package com.globallogic.challenge.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestController
@RestControllerAdvice
@Slf4j
public class ResponseExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ExceptionResponse handleAll(Exception ex) {
        return constructException(ex);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ExceptionResponse handlerIllegalArgumentException(ConstraintViolationException ex) {
        return constructException(ex);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ExceptionResponse handlerBadRequestException(BadRequestException ex) {
        return constructException(ex);
    }

    @ExceptionHandler(UniqueEmailException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public final ExceptionResponse handleUniqueEmailException(UniqueEmailException ex) {
        return constructException(ex);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public final ExceptionResponse handleEntityNotFoundException(EntityNotFoundException exception) {
        return constructException(exception);
    }

    @ExceptionHandler(EntityAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public final ExceptionResponse handlerEntityAlreadyExistsException(EntityAlreadyExistsException ex) {
        return constructException(ex);
    }

    private ExceptionResponse constructException(Exception ex) {
        log.debug("ERROR: " + ex.getClass().getName(), ex);
        log.error("ERROR: " + ex.getClass().getName() + ". Message: " + ex.getMessage());
        if (ex instanceof ConstraintViolationException) {
            return new ExceptionResponse(((ConstraintViolationException) ex).getConstraintViolations().stream().findFirst().get().getMessageTemplate());
        }
        return new ExceptionResponse(ex.getMessage());
    }
}
