package com.globallogic.challenge.exceptions;

import lombok.Data;

import java.time.Instant;

@Data
public class ExceptionResponse {

    private String message;

    public ExceptionResponse(String message) {
        this.message = message;
    }
}
