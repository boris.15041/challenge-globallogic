# Challenge JAVA - Global Logic

## Descripción del challenge
Desarrollo de una aplicación que expone una API RESTful para la gestión de usuarios.

#### Requerimientos exigidos

* Base de datos H2
* Gestor de dependencia con Gradle
* Pruebas Unitarias con JUnit
* Persistencia con Hibernate
* Framework Spring Boot
* Lenguaje de programación JAVA 8
* Repositorio del código fuente en Github
* Diagrama de secuencia en UML 
* Diagrama de componentes en UML
* Token con JWT

## Descargar el proyecto desde el repositorio

El repositorio del proyecto es de acceso público. Por lo que solo se tiene que clonar en su máquina. Para ello previamente debe tener instalado git.

Ubicarse en el directorio en el cual quiere descargar el proyecto y ejecutar el siguiente comando, ya sea Windows o Linux

```bash
[user@linux ~]$ git clone https://gitlab.com/boris.15041/challenge-globallogic.git
[user@linux ~]$ cd challenge-globallogic
```

## Requisitos para levantar el proyecto
Se debe tener instalado la versión de java 8 o superior en el sistema operativo.

En cuanto a base de datos, al usar H2 no es necesario realizar ninguna configuración adicional para este proyecto.

Abrir una terminal y navegar a la raíz del proyecto. Ejecutar el siguiente comando dependiendo del sistema operativo:

```bash
[user@linux ~]$ ./gradlew bootRun
```
```bash
[windows]$ gradlew bootRun
```

Esto permite levantar al proyecto y consumir los servicios expuestos.

Para visualizar los endpoints de la API se implementa Swagger. Para acceder a esta interfaz, en el navegador web ingresar el siguiente enlace siempre y cuando el proyecto esté levantado:


[http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/)

Los endpoints no están securizados, por lo que se puede realizar las pruebas desde Swagger.

El proyecto ya contiene algunos datos de prueba en la base de datos.

## Ejecutar las Pruebas Unitarias
Para ejecutar las pruebas unitarias, debe navegar hasta la raíz del proyecto y ejecutar el siguiente comando dependiendo del sistema operativo

```bash
[user@linux ~]$ ./gradlew test
```
```bash
[windows]$ gradlew test
```
Si desea ver los resultados generales de las pruebas, puede ejecutar la siguiente linea en linux
```bash
[user@linux ~]$ google-chrome build/reports/tests/test/index.html
```
En windows puede navegar hasta la carpeta, partiendo desde la raiz del proyecto hasta "build/reports/tests/test/" y abrir el archivo "index.html" con su navegador web preferido.

## Despliegue de proyecto
El proyecto se encuentra desplegado mediante docker en un servidor para que consuman los servicios expuestos.
Los endpoints no están securizados, se puede entonces realizar las pruebas desde Swagger mediante el
siguiente link sin la necesidad de levantar el proyecto:

[http://moggix.tech:8080/swagger-ui/](http://moggix.tech:8080/swagger-ui/)

## Diagrama  de Componentes y de Secuencia
Se muestra a continuación el diagrama de componentes

![alt text for screen readers](src/main/resources/static/images/diagrama-de-componentes.png "Diagrama de secuencia")

El diagrama de secuencias de agregar un nuevo usuario en el servicio

![alt text for screen readers](src/main/resources/static/images/diagrama-de-secuencia.png "Diagrama de secuencia")
